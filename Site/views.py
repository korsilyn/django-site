from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from Django.settings import STATICFILES_DIRS
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm

def get_base(request):
    '''
    Return base args for all site pages

    :param request: user request
    :return: args dict
    '''

    return {
        'request': request,
        'user': request.user,
    }


def index(request):
    '''
    Main site page

    :param request: user request
    :return: main page
    '''

    return render(request, 'index.html', get_base(request))
